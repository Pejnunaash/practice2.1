// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShoter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TopDownShoter, "TopDownShoter" );

DEFINE_LOG_CATEGORY(LogTopDownShoter)
 