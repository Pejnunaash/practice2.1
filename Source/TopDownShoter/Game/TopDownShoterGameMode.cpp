// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShoterGameMode.h"
#include "TopDownShoterPlayerController.h"
#include "../Character/TopDownShoterCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopDownShoterGameMode::ATopDownShoterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDownShoterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
///Game/Blueprint/Character/TopDownCharacter
//Blueprint'/Game/Blueprint/Character/BP_Character.BP_Character'