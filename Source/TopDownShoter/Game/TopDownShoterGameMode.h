// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TopDownShoterGameMode.generated.h"

UCLASS(minimalapi)
class ATopDownShoterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATopDownShoterGameMode();
};



